package com.vismash.exercise.concurrence;


public class LoginServlet {

    private static String usernameRef;
    private static String passwordRef;

    public static void doPostNotThreadSafe(String username, String password) {
        logIn(username, password);
    }

    synchronized public static void doPostThreadSafe(String username, String password) {
        logIn(username, password);
    }

    private static void logIn(String username, String password) {
        try {
            usernameRef = username;

            if (username.equals("a")) {
                Thread.sleep(5000);
            }

            passwordRef = password;
            System.out.println("username=" + usernameRef + " password=" + password);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
