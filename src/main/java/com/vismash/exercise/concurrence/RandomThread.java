package com.vismash.exercise.concurrence;

public class RandomThread implements Runnable {

    @Override
    public void run() {
        System.out.println("运行中");
    }


    public static void main(String[] args) {
        RandomThread rt = new RandomThread();
        Thread thread = new Thread(rt);
        thread.start();
        System.out.println("运行结束");
    }
}
