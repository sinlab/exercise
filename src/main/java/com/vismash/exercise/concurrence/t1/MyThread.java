package com.vismash.exercise.concurrence.t1;

public class MyThread implements Runnable{
    private Thread mt = new Thread(this);

    public MyThread() {
        System.out.println("MyThread---beagin");
        System.out.println("Thread.currentThread().getName()=" + Thread.currentThread().getName());
        System.out.println("Thread.currentThread().isAlive()=" + Thread.currentThread().isAlive());
        System.out.println("this.getName()=" + mt.getName());
        System.out.println("this.isAlive()=" + mt.isAlive());
        System.out.println("MyThread---end");
    }

    @Override
    public void run() {
        System.out.println("run---beagin");
        System.out.println("Thread.currentThread().getName()=" + Thread.currentThread().getName());
        System.out.println("Thread.currentThread().isAlive()=" + Thread.currentThread().isAlive());
        System.out.println("this.getName()=" + mt.getName());
        System.out.println("this.isAlive()=" + mt.isAlive());
        System.out.println("run---end");
    }
}
