package com.vismash.exercise.concurrence.t1;

public class Run {
    public static void main(String[] args) {
        MyThread mythread = new MyThread();
        Thread mt = new Thread(mythread);

        System.out.println("main begin mt isAlive=" + mt.isAlive());
        mt.setName("A");
        mt.start();

        System.out.println("main end mt is Alive=" + mt.isAlive());
    }
}
