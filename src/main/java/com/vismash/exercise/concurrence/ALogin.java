package com.vismash.exercise.concurrence;

public class ALogin implements Runnable{
    @Override
    public void run() {
//        LoginServlet.doPostNotThreadSafe("a", "aa");
        LoginServlet.doPostThreadSafe("a", "aa");
    }
}
