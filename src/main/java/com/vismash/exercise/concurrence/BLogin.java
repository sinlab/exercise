package com.vismash.exercise.concurrence;

public class BLogin implements Runnable{

    @Override
    public void run() {
//        LoginServlet.doPostNotThreadSafe("b", "bb");
        LoginServlet.doPostThreadSafe("b", "bb");
    }
}
