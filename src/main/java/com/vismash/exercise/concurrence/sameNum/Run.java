package com.vismash.exercise.concurrence.sameNum;

public class Run {
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        Thread mt = new Thread(myThread);

        Thread t1 = new Thread(mt);
        Thread t2 = new Thread(mt);
        Thread t3 = new Thread(mt);
        Thread t4 = new Thread(mt);
        Thread t5 = new Thread(mt);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}
