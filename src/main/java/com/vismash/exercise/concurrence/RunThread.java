package com.vismash.exercise.concurrence;

public class RunThread {
    public static void main(String[] args) {
        ALogin a = new ALogin();
        Thread alogin = new Thread(a);
        alogin.start();

        BLogin b = new BLogin();
        Thread blogin = new Thread(b);
        blogin.start();
    }
}
